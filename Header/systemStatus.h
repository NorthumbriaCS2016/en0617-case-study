enum{
  systemStarting = 4,
  systemReady,
  systemRunning,
  systemPausing,
  systemPaused,
  systemStopping,
  systemStopped,
  errorShutdown,
  };