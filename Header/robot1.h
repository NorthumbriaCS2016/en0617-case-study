/*
 * Constants For Robot1
 * 2016 
 * @Author Christopher Hayhurst
 */

//robot statuses
enum{
  IDLE = 0,
  READY_TO_PICKUP,
  MOVING,
  READY_TO_DROPOFF,
  SHUTDOWN,
  PAUSE
};

/************************  Robot 1 Coordinates  ********************************/

//READY_TO_PICKUP
enum{
  RDY_HAND = 45000,
  RDY_WRIST = 90000,
  RDY_WAIST = 85000
};

//Down for pickup 
enum{
  PU_HAND = 90000,
  PU_ELBOW = 100000
};

//Pre drop off
enum{
  PDO_WAIST = 45000,
  PDO_ELBOW = 90000
};

//Drop off
enum{
  DO_ELBOW = 100000,
  DO_HAND = 45000
};

//After drop off
  enum{
  ADO_ELBOW = 90000,
  ADO_HAND = 45000
};
  
  