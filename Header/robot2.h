/*
 * Constants For Robot2
 * 2016 
 * @Author Stefano Mantini
 */

// global delay on robot movements
uint32_t RbtDly = 10;

//robot statuses
enum{
  IDLE = 0,
  READY_TO_PICKUP,
  MOVING,
  MOVINGBACK,
  READY_TO_DROPOFF,
  SHUTDOWN,
  PAUSE,
  PICKING_UP,
  STOPPED, 
  ERROR,
  START
};

/************************  Robot Coordinates  ********************************/

//READY_TO_PICKUP
enum{
  RDY_HAND = 45000,
  RDY_WRIST = 100000,
  RDY_WAIST = 50000
};

//Pickup 
enum{
  PU_HAND = 100000,
  PU_ELBOW = 96000,
};

//PostPickup
enum{
  PPU_ELBOW = 80000,
};
  
//pre dropoff
enum{
  PreDO_WAIST = 100000
};

//dropoff
enum{
  DO_HAND = 45000,
  DO_ELBOW = 96500,
};

//post dropoff
enum{
  PostDO_ELBOW = 90000
};