/* CAN Receiver
*
* Poll CAN 1 every 20ms and toggle interface LED D1 on reception
*
*/

#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <can.h>
#include <leds.h>
#include <lcd.h>
#include <interface.h>
#include <control.h>
#include <buttons.h>
#include <constants.h>
#include <systemStatus.h>
#include <robots.h>

/*************************************************************************
*                  PRIORITIES
*************************************************************************/

enum{
  APP_TASK_BROADCAST_PRIO=4,
  APP_TASK_INTERFACE_PRIO ,
  APP_TASK_CAN_RECEIVE_PRIO,
  APP_TASK_SENSOR2_PRIO
    
};

/*************************************************************************
*                  APPLICATION TASK STACKS
*************************************************************************/

enum {APP_TASK_CAN_RECEIVE_STK_SIZE = 256,
APP_TASK_BROADCAST_STK_SIZE = 256,
APP_TASK_SENSOR2_STK_SIZE = 256,
APP_TASK_INTERFACE_STK_SIZE = 256
};

static OS_STK appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE];
static OS_STK appTaskBroadcastStk[APP_TASK_BROADCAST_STK_SIZE];
static OS_STK appTaskSensor2Stk[APP_TASK_SENSOR2_STK_SIZE];
static OS_STK appTaskInterfaceStk[APP_TASK_INTERFACE_STK_SIZE];

/*************************************************************************
*                  APPLICATION FUNCTION PROTOTYPES
*************************************************************************/

static void appTaskCanReceive(void *pdata);
static void appTaskBroadcast(void *pdata);
static void appTaskSensor2(void *pdata);

static void appTaskInterface(void *pdata);

static bool r1Paused = false; 
static bool r2Paused = false;
static bool convPaused = false;
static bool conveyActive = false;
static bool robot1Active = false;
static bool robot2Active = false;
static bool conveyReady = false;
static bool Rob1Ready = false;
static bool Rob2Ready = false;
static bool r2Stopped = false;
bool blocksActive = false;
bool conveyor1= false;
bool conveyor2 =false;
bool conveyorBelt = false;

uint32_t robot2Status;
uint32_t robot1Status;
uint32_t conveyorStatus;



uint32_t pad1;
uint32_t pad2;
uint32_t count = 0;

uint32_t systemStatus = systemStarting;
/*************************************************************************
*                    GLOBAL FUNCTION DEFINITIONS
*************************************************************************/

int main() {
  /* Initialise the hardware */
  bspInit();
  interfaceInit(NO_DEVICE);
  
  /* Initialise the OS */
  OSInit();                                                   
  
  /* Create Tasks */
  OSTaskCreate(appTaskCanReceive,                               
               (void *)0,
               (OS_STK *)&appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE - 1],
               APP_TASK_CAN_RECEIVE_PRIO);
  
  OSTaskCreate(appTaskBroadcast,                               
               (void *)0,
               (OS_STK *)&appTaskBroadcastStk[APP_TASK_BROADCAST_STK_SIZE - 1],
               APP_TASK_BROADCAST_PRIO);
  
  OSTaskCreate(appTaskSensor2,                               
               (void *)0,
               (OS_STK *)&appTaskSensor2Stk[APP_TASK_SENSOR2_STK_SIZE - 1],
               APP_TASK_SENSOR2_PRIO);
  
  OSTaskCreate(appTaskInterface,                               
               (void *)0,
               (OS_STK *)&appTaskInterfaceStk[APP_TASK_INTERFACE_STK_SIZE - 1],
               APP_TASK_INTERFACE_PRIO);
  
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*************************************************************************
*                   APPLICATION TASK DEFINITIONS
*************************************************************************/
static void appTaskInterface(void *pdata){
  /*systemStarting = 4,
  systemReady,
  systemActive,
  systemRunning,
  systemPausing,
  systemPaused,
  systemUnpaused,
  systemStopping,
  systemStopped,
  errorShutdown,*/
  while(true){
    /* Check Cells are reday to start */
    
    lcdSetTextPos(2,2);
    lcdWrite("System: %d", systemStatus);
    lcdSetTextPos(2,3);
    lcdWrite("Robot1: %d", robot1Status);
    lcdSetTextPos(2,4);
    lcdWrite("Robot2: %d", robot2Status);
    lcdSetTextPos(2,5);
    lcdWrite("Convey: %d", conveyorStatus);
    
    
    if(systemStatus == systemStarting || systemStatus == systemReset)
    {
      interfaceLedToggle(D1_LED);
      interfaceLedSetState(D4_LED, LED_OFF);
    } 
    
    if((conveyorStatus == conveyorReady) &&(robot1Status == ro1Ready) && (robot2Status == ro2Ready) && (systemStatus == systemStarting || systemStatus == systemReset))
    {
      systemStatus = systemReady;
    }
    if(systemStatus == systemReady)
    {
      interfaceLedSetState(D1_LED, LED_ON);
    } 
    
    if(systemStatus == systemActive){
      interfaceLedToggle(D2_LED);
    }
    
    /* Start the System */
    
    if((robot1Status == ro1ReadyToPickup) && (robot2Status == ro2ReadyToPickup) && (systemStatus == systemActive)){
      systemStatus = systemRunning;
    }
    
    if(systemStatus == systemRunning)
    {
      interfaceLedSetState(D1_LED, LED_OFF);
      interfaceLedSetState(D2_LED, LED_ON);
      interfaceLedSetState(D3_LED, LED_OFF);
      interfaceLedSetState(D4_LED, LED_OFF);
    }
     
    /* Emergency Shutdown */
    if(controlEmergencyStopButtonPressed())
    {
      systemStatus = errorShutdown;
      
    }
    
    if(systemStatus == errorShutdown){
      interfaceLedSetState(D1_LED, LED_OFF);
      interfaceLedSetState(D2_LED, LED_OFF);
      interfaceLedSetState(D3_LED, LED_OFF);
      interfaceLedSetState(D4_LED, LED_ON);   
    } 
    
    if(systemStatus == systemStopping){
      interfaceLedToggle(D2_LED);
    } 
    
    if((systemStatus == systemStopping) && (robot2Status == ro2Stop )&& (robot1Status == ro1Stop )){
      systemStatus = systemStopped; 
    }
    
    if(systemStatus == systemStopped)
    {
      interfaceLedSetState(D2_LED, LED_OFF);
    }
    if(systemStatus == systemRunning){
      interfaceLedSetState(D2_LED, LED_ON);
    }
    
    /* Start/stop the System */
    if(isButtonPressed(BUT_1)){
      if(systemStatus == systemReady){
        systemStatus = systemActive;
        interfaceLedSetState(D1_LED, LED_OFF);
        interfaceLedToggle(D2_LED);
      }else if(systemStatus == systemRunning){
        systemStatus = systemStopping;
      }else if(systemStatus == systemStopped){
        systemStatus = systemActive;
      }
    }
    
    if(conveyor1 || conveyor2 || conveyorBelt){
      blocksActive = true;
    } else{
      blocksActive = false;
    }
    
    /* Pause/Unpause System */
    if(isButtonPressed(BUT_2)){
      if(systemStatus == systemRunning){
        systemStatus = systemPausing;  
      }
      if(systemStatus == systemPaused){
        systemStatus = systemUnpausing;
      }
    }
    
    if(systemStatus == systemUnpausing){
      interfaceLedToggle(D3_LED);   
    }
    
    if(systemStatus == systemUnpausing && (!(robot1Status == ro1Pause)) &&  (!(robot2Status == ro2Pause))){
      
      systemStatus = systemRunning;
    }
    
    if(systemStatus == systemPausing && (robot2Status == ro2Pause) && (robot1Status == ro1Pause))
    {
      systemStatus = systemPaused;
    }
    
    if(systemStatus == systemPaused)
    {
      interfaceLedSetState(D2_LED, LED_OFF);  
      interfaceLedSetState(D3_LED, LED_ON);
    }
    if(systemStatus == systemPausing){
      interfaceLedToggle(D3_LED);
      interfaceLedSetState(D1_LED, LED_OFF);
      interfaceLedSetState(D2_LED, LED_OFF);
      interfaceLedSetState(D4_LED, LED_OFF);
    }
    
    if(isButtonPressed(JS_UP) && (systemStatus == errorShutdown)){
      systemStatus = systemReset;
    }
      
    /***
    ***********************************************************************
    Code below is used to test individual cell
    
    ***********************************************
    */

    if(isButtonPressed(JS_CENTRE)){
      systemStatus = systemReady;
    }
    if(isButtonPressed(JS_LEFT)){
      if(convPaused == false){
        convPaused = true;

      }else{
        convPaused = false;
      }
    }
    if(isButtonPressed(JS_RIGHT)){
      systemStatus = systemRunning;
    }
    if(isButtonPressed(JS_DOWN)){
      //bspInit();
      }
    OSTimeDly(250);
  }
  
}


static void appTaskCanReceive(void *pdata) {
  canMessage_t msg;
  
  while ( true ) 
  {
    if (canReady(CAN_PORT_1)) 
    {  
      canRead(CAN_PORT_1, &msg);
      
      /* Emergency Shutdown Received */
      if(msg.id == emergencyShutdown){
        systemStatus = errorShutdown;
        interfaceLedSetState(D2_LED, LED_OFF);
        interfaceLedSetState(D1_LED, LED_OFF);
        interfaceLedSetState(D4_LED, LED_ON);
      }
      if(msg.id == convReady){
        conveyorStatus = conveyorReady;
      }
      
      /*Robot 2 Status*/
      if(msg.id == robot2Ready){
        robot2Status = ro2Ready;
      }
      if(msg.id == robot2Stop){
        robot2Status = ro2Stop;
      }
      
      if(msg.id == robot2ReadyToPickup){
        robot2Status = ro2ReadyToPickup;
      }
       if(msg.id == robot2Moving)
       {
        robot2Status = ro2Moving;
      }
      if(msg.id == robot2ReadyToDropoff){
        robot2Status = ro2ReadyToDropoff;
      }
      if(msg.id == robot2Shutdown){
        robot2Status = ro2Shutdown;
      }
      if(msg.id == robot2Pause){
        robot2Status = ro2Pause; 
      }
      
      /*Robot 1 Status*/
      if(msg.id == robot1Stop){
        robot1Status = ro1Stop;
      }
      if(msg.id == robot1Ready){
        robot1Status = ro1Ready;
      }
      if(msg.id == robot1ReadyToPickup){
        robot1Status = ro1ReadyToPickup;
      }
       if(msg.id == robot1Moving)
       {
        robot1Status = ro1Moving;
      }
      if(msg.id == robot1ReadyToDropoff){
        robot1Status = ro1ReadyToDropoff;
      }
      if(msg.id == robot1Shutdown){
        robot1Status = ro1Shutdown;
      }
      if(msg.id == robot1Pause){
        robot1Status = ro1Pause; 
      }
      
      /* Check all cells are paused */
      if(msg.id == conveyorPaused){
        convPaused = true;
      }
      
      if(msg.id == error){
         systemStatus = errorShutdown;
      }
      
      if(msg.id == convSensor2Active){
        conveyorStatus = true;
      }
      if(msg.id == convSensor2Empty){
        conveyorStatus = false;
      }
    } 
    OSTimeDly(100);
  }
}


static void appTaskSensor2(void *pdata){
  while(true){ 
    if(controlItemPresent(CONTROL_SENSOR_1)&& (systemStatus != systemStopping))
    {
      pad1 = pad1Full;
    }else if (!controlItemPresent(CONTROL_SENSOR_1)){
      pad1 = pad1Zero;
    }
    
    if(controlItemPresent(CONTROL_SENSOR_2)){
      pad2 = pad2Full;
    } else if(!controlItemPresent(CONTROL_SENSOR_2)){
      pad2 =pad2Zero;
    }
    OSTimeDly(100);
  }
}


static void appTaskBroadcast(void *pdata){
  canMessage_t msg = {0, 0, 0, 0};
  osStartTick();
  
  while ( true ) {

    if(systemStatus == systemReset){
      msg.id = resetSystem;
      canWrite(CAN_PORT_1, &msg);
    }
    /* Broadcast System Start */
    if(systemStatus == systemActive){
      msg.id = systemStart;
      canWrite(CAN_PORT_1, &msg);
    }
    
    /* Broadcast System Stop */
    if(systemStatus == systemStopping && (!blocksActive)){
      msg.id = systemStop;
      canWrite(CAN_PORT_1, &msg);
    }
    
    /* Broadcast System Shutdown */
    if(systemStatus == errorShutdown){
      msg.id = emergencyShutdown;
      canWrite(CAN_PORT_1, &msg);
      count++;

    }
    
    /* Begin Puasing the System*/
    if(systemStatus == systemPausing){
      msg.id = pauseSystem;
      canWrite(CAN_PORT_1, &msg);
    }
    
    /* Unpause the System */
    if(systemStatus == systemUnpausing){
      msg.id = unPauseSystem;
      canWrite(CAN_PORT_1, &msg);
    }
    
    
    /* Broadcast Sensor Status */
    if(systemStatus == systemRunning){
      if(pad1 == pad1Full)
      {
        msg.id = pad1Active;
        canWrite(CAN_PORT_1, &msg);
      } else if(pad1 == pad1Zero){
        msg.id = pad1Empty;
        canWrite(CAN_PORT_1, &msg);
      }
      if(pad2 == pad2Full){
        count++;
        msg.id = pad2Active;
        canWrite(CAN_PORT_1, &msg);
      } else if(pad2 == pad2Zero){
        msg.id = pad2Empty;
        canWrite(CAN_PORT_1, &msg);
      }
      
    }
    
    OSTimeDly(175);
  }
  
  
}
