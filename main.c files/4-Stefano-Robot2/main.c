/* CAN Receiver
*
* Poll CAN 1 every 20ms and toggle interface LED D1 on reception
*
*/

#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <can.h>
#include <leds.h>
#include <lcd.h>
#include <interface.h>
#include <robot.h>
#include <buttons.h>
#include <constants.h>
#include <robot2.h>
/*************************************************************************
*                  PRIORITIES
*************************************************************************/

enum {
  APP_TASK_CAN_RECEIVE_PRIO =4,
  APP_TASK_CAN_SEND_PRIO,
  APP_TASK_ROBOT_PRIO
};

/*************************************************************************
*                  APPLICATION TASK STACKS
*************************************************************************/

enum {APP_TASK_CAN_RECEIVE_STK_SIZE = 256};
enum {APP_TASK_CAN_SEND_STK_SIZE = 256};
enum {APP_TASK_ROBOT_STK_SIZE = 256};

static OS_STK appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE];
static OS_STK appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE];
static OS_STK appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE];
/*************************************************************************
*                  APPLICATION FUNCTION PROTOTYPES
*************************************************************************/

static void appTaskCanReceive(void *pdata);
static void appTaskCanSend(void *pdata);
static void appTaskRobot(void *pdata);
static OS_EVENT *PauseSem;


//local vars from globals
bool shutdown           = false;
bool pause              = false;
bool cnvS1Status        = false;
bool cnvS2Status        = false;
bool pad1Status         = false; 
bool pad2Status         = false; 
bool init               = false;
uint32_t iterations     =0;
uint32_t robot2Status   = 0;
uint32_t pollMax        = 5;
uint32_t pollStatus     = 0;
uint8_t PauseStatus;
/*************************************************************************
*                    GLOBAL FUNCTION DEFINITIONS
*************************************************************************/

int main() {
  /* Initialise the hardware */
  bspInit();
  
  interfaceInit(NO_DEVICE);
  robotInit();
  
  /* Initialise the OS */
  OSInit();                                                   
  
  /* Create Tasks */
  OSTaskCreate(appTaskCanReceive,                               
               (void *)0,
               (OS_STK *)&appTaskCanReceiveStk[APP_TASK_CAN_RECEIVE_STK_SIZE - 1],
               APP_TASK_CAN_RECEIVE_PRIO);
  
  OSTaskCreate(appTaskCanSend,                               
               (void *)0,
               (OS_STK *)&appTaskCanSendStk[APP_TASK_CAN_SEND_STK_SIZE - 1],
               APP_TASK_CAN_SEND_PRIO);
  
  OSTaskCreate(appTaskRobot,                               
               (void *)0,
               (OS_STK *)&appTaskRobotStk[APP_TASK_ROBOT_STK_SIZE - 1],
               APP_TASK_ROBOT_PRIO);
  
  //semaphore for pause
  PauseSem = OSSemCreate(1);
  
  
  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*************************************************************************
*                   APPLICATION TASK DEFINITIONS
*************************************************************************/


static void appTaskRobot(void *pdata) {
  while(true){ //main loop    
    //if its the first time looping
    if(iterations == 0){
      //init msg broadcast
      canMessage_t msg = {0, 0, 0, 0};
      msg.id = robot2Ready;
      canWrite(CAN_PORT_1, &msg);
    }
    
    
    //system start/stopped here if it is not the first time  iterating through
    if(!init && iterations>=1){
      canMessage_t msg = {0, 0, 0, 0};
      msg.id = robot2Ready;
      canWrite(CAN_PORT_1, &msg);      
    }
    // if start/stop message is sent from control
    if(init){
      
      //robot status
      robot2Status = IDLE;
      /**
      * default position is 
      * Awaiting pad2Status
      */
      if(!shutdown){
        if(pause){
          robot2Status = PAUSE;
          OSSemPend(PauseSem, 0, &PauseStatus);
        } else {
          //Open hand
          if(!shutdown){
          while((robotJointGetState(ROBOT_HAND)) > RDY_HAND){
            robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
            OSTimeDly(RbtDly);
          }
          }else{
            robot2Status = SHUTDOWN;
          }

        }
        if(pause){
          robot2Status = PAUSE;
          OSSemPend(PauseSem, 0, &PauseStatus);
        } else {
          if(!shutdown){
          // Move laterally to conveyor
          while((robotJointGetState(ROBOT_WAIST)) > RDY_WAIST){
            robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_DEC);
            OSTimeDly(RbtDly);
          }
          }else{
            robot2Status = SHUTDOWN;
          }

        }
        
        if(pause){
          robot2Status = PAUSE;
          OSSemPend(PauseSem, 0, &PauseStatus);
        } else {
          if(!shutdown){
            //move wrist to ready to pickup
            while((robotJointGetState(ROBOT_WRIST)) < RDY_WRIST){
              robotJointSetState(ROBOT_WRIST, ROBOT_JOINT_POS_INC);
              OSTimeDly(RbtDly);
            }    
          }else{
            robot2Status = SHUTDOWN;
          }
        }
        
      }
      
      
      
      
      if(shutdown){
        /*deadlock here*/
        robot2Status = SHUTDOWN;
      }
      robot2Status = READY_TO_PICKUP;
      
      //block here if paused and notify of pause
      if(pause){
        robot2Status = PAUSE;
        canMessage_t msg; //init can
        msg.id = robot2Pause;
        canWrite(CAN_PORT_1, &msg);
      }
      //pick up block & move to ready to dropoff  
      if(cnvS2Status && !pause && !shutdown){
        
        //Set robot2Status to moving
        robot2Status= MOVING;
        
        //should have picked up at this point, check and retry if not
        pollStatus = 0;
        while(pollStatus <= pollMax && cnvS2Status){
          //go down to pick up
          
          if(pause){
            robot2Status = PAUSE;
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            //Ensure hand is open hand
            if(!shutdown){
              while((robotJointGetState(ROBOT_HAND)) > RDY_HAND){
                robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
                OSTimeDly(RbtDly);
              }
            }else{
              robot2Status = SHUTDOWN;
            }
            
          }
          
          if(pause){
            robot2Status = PAUSE;
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            //move down
            if(!shutdown){
              while((robotJointGetState(ROBOT_ELBOW)) < PU_ELBOW){
                robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
                OSTimeDly(RbtDly);
              }    
            }else{
              robot2Status = SHUTDOWN;
            }
            
          }
          
          if(pause){
            robot2Status = PAUSE;
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            if(!shutdown){
              //Close hand
              while((robotJointGetState(ROBOT_HAND)) < PU_HAND){
                robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_INC);
                OSTimeDly(RbtDly);
              }
            }else{
              robot2Status = SHUTDOWN;
            }
            
          }
          
          if(pause){
            robot2Status = PAUSE;
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            if(!shutdown){
              //Move up slightly to clear pad
              while((robotJointGetState(ROBOT_ELBOW)) > PPU_ELBOW){
                robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
                OSTimeDly(RbtDly);
              }
            }else{
              robot2Status = SHUTDOWN;
            }
            
          }
          OSTimeDly(4000);
          pollStatus++;
        }
        if (pollStatus >= pollMax){
          canMessage_t msg = {0, 0, 0, 0};
          msg.id = emergencyShutdown;
          canWrite(CAN_PORT_1, &msg);
          robot2Status = SHUTDOWN;
        }else{
          
          if(pause){
            robot2Status = PAUSE;
            OSSemPend(PauseSem, 0, &PauseStatus);
          } else {
            if(!shutdown){
              //move laterally to PRE-dropoff position
              while((robotJointGetState(ROBOT_WAIST)) < PreDO_WAIST){
                robotJointSetState(ROBOT_WAIST, ROBOT_JOINT_POS_INC);
                OSTimeDly(RbtDly);
              }   
            }else{
              robot2Status = SHUTDOWN;
            }
            
          }
          
          //Set robot2Status to ready to dropoff
          robot2Status= READY_TO_DROPOFF;
          
          //catch stopped
          
          
          /**
          * Dropoff block
          */
          //wait X seconds for 
          pollStatus=0;
          while(pollStatus < pollMax){
            //dropoff when nothing on pad
            if(pad2Status == 0){
              //GO DOWN
              if(pause == 0 && shutdown == 0){
                while((robotJointGetState(ROBOT_ELBOW)) < DO_ELBOW){
                  if(pause){
                    OSSemPend(PauseSem, 0, &PauseStatus);
                  } else {
                    if(!shutdown){
                      robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_INC);
                      OSTimeDly(RbtDly);
                    }else{
                      robot2Status = SHUTDOWN;
                    }
                  }
                } 
              }
              //RELEASE BLOCK FROM HAND
              if(pause == 0 && shutdown == 0){
                while((robotJointGetState(ROBOT_HAND)) > DO_HAND){
                  if(pause){
                    OSSemPend(PauseSem, 0, &PauseStatus);
                  } else {
                    if(!shutdown){
                      robotJointSetState(ROBOT_HAND, ROBOT_JOINT_POS_DEC);
                      OSTimeDly(RbtDly);
                    }else{
                      robot2Status = SHUTDOWN;
                    }
                  }
                }
              }
              //MOVE UP TO CLEAR PAD
              if(pause == 0 && shutdown == 0){
                while((robotJointGetState(ROBOT_ELBOW)) > PostDO_ELBOW){
                  if(pause){
                    OSSemPend(PauseSem, 0, &PauseStatus);
                  } else {
                    if(!shutdown){
                      robotJointSetState(ROBOT_ELBOW, ROBOT_JOINT_POS_DEC);
                      OSTimeDly(RbtDly);
                    }else{
                      robot2Status = SHUTDOWN;
                    }
                  }
                }
              }
              //update robot status
              robot2Status= 3;
              //get out of loop if successful
              pollStatus =99;
              break;
            }else if(pollStatus >=pollMax){
              //error shutdown
              shutdown=true;
              lcdSetTextPos(10, 10);
              lcdWrite("ERROR");
              
            }//end else if
            robot2Status= PICKING_UP;
            OSTimeDly(1000);
            pollStatus++;
            iterations++;
          } //end while       
        }// end else
        
        
        
      }
      OSTimeDly(1000);
    }
  }
}


static void appTaskCanReceive(void *pdata) {
  canMessage_t rxMsg; //init can
  
  //main loop
  while(true){
    
    //Buttons toggle statuses
    if(isButtonPressed(BUT_1)){
      init = !init;
    }
    if(isButtonPressed(BUT_2)){
    }
    if(isButtonPressed(JS_UP)){
      cnvS2Status = !cnvS2Status;
    }
    if(isButtonPressed(JS_DOWN)){
      pad2Status = !pad2Status;
    }
    if(isButtonPressed(JS_LEFT)){
      shutdown = !shutdown;
    }
    if(isButtonPressed(JS_RIGHT)){
      pause = !pause;
    }
    
    // If CAN1 is ready
    if (canReady(CAN_PORT_1)) {
      //read can messages and update local vars
      canRead(CAN_PORT_1, &rxMsg);
      
      if(rxMsg.id==convSensor2Active){
        cnvS2Status = true;
      }
      if(rxMsg.id==convSensor2Empty){
        cnvS2Status = false;
      }
      if(rxMsg.id==emergencyShutdown){
        shutdown = true;
      }
      if(rxMsg.id==pauseSystem){
        pause = true;
      }
      if(rxMsg.id==unPauseSystem){
        pause = false;
        PauseStatus = OSSemPost(PauseSem);
        canMessage_t msg = {0, 0, 0, 0};
        msg.id = robot2UnPause;
        canWrite(CAN_PORT_1, &msg);
      }
      if(rxMsg.id==pad1Active){
        pad1Status = true;
      }
      if(rxMsg.id==pad1Empty){
        pad1Status = false;
      }
      
      if(rxMsg.id==pad2Active){
        pad2Status = true;
      }
      if(rxMsg.id==pad2Empty){
        pad2Status = false;
      }
      if(rxMsg.id==convSensor1Active){
        cnvS1Status = true;
      }
      if(rxMsg.id==convSensor1Empty){
        cnvS1Status = false;
      }
      if(rxMsg.id==systemStart){
        init = true;
      }
      if(rxMsg.id==systemStop){
        init = false;
      }
      
      lcdSetTextPos(1, 1);
      lcdWrite("INIT: %02d", init);
      lcdSetTextPos(1, 2);
      lcdWrite("SHDO: %02d", shutdown);
      lcdSetTextPos(1, 3);
      lcdWrite("PAUS: %02d", pause);
      lcdSetTextPos(1, 4);
      lcdWrite("CNV1: %02d", cnvS1Status);
      lcdSetTextPos(1, 5);
      lcdWrite("CNV2: %02d", cnvS2Status);
      lcdSetTextPos(1, 6);
      lcdWrite("ROB2: %02d", robot2Status);
      lcdSetTextPos(1, 7);
      lcdWrite("PAD1: %02d", pad1Status);
      lcdSetTextPos(1, 8);
      lcdWrite("PAD2: %02d", pad2Status);
      lcdSetTextPos(1, 9);
      lcdWrite("POLL: %02d", pollStatus);
      lcdSetTextPos(1, 10);
      lcdWrite("ITER: %02d", iterations);
      
    }
    
    OSTimeDly(100);
  }
}


static void appTaskCanSend(void *pdata) {
  osStartTick(); //start OS
  //init can msg struct
  canMessage_t msg = {0, 0, 0, 0};
  
  //main loop
  while ( true ) {
    //moving
    if(robot2Status == READY_TO_PICKUP){
      msg.id = robot2ReadyToPickup;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot2Status == MOVING){
      msg.id = robot2Moving;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot2Status == READY_TO_DROPOFF){
      msg.id = robot2ReadyToDropoff;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot2Status == SHUTDOWN){
      msg.id = emergencyShutdown;
      canWrite(CAN_PORT_1, &msg);
    }
    else if(robot2Status == PAUSE){
      msg.id = robot2Pause;
      canWrite(CAN_PORT_1, &msg);
    }
    if(shutdown){
      msg.id = emergencyShutdown;
      canWrite(CAN_PORT_1, &msg);
    }
    
    OSTimeDly(500);
  }
}