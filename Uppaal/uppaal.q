//This file was generated from (Commercial) UPPAAL 4.0.14 (rev. 5615), May 2014

/*
Cell 3 Test
*/
A[](\
	!(I.cnvStatus2 == 1 && I.robot2Status ==2 && I.pad2Sensor == 1)\
)

/*
Cell 3 Test
*/
A[](\
	!(I.cnvStatus2 == 1 && I.robot2Status ==1 && I.pad2Sensor == 1)\
)

/*
Cell 3 Test
*/
A[](\
	!(I.cnvStatus2 == 0 && I.robot2Status ==2 && I.pad2Sensor == 1)\
)

/*
Cell 3 Test
*/
A[](\
	!(I.cnvStatus2 == 0 && I.robot2Status ==1 && I.pad2Sensor == 1)\
)

/*

*/
A[](\
	!(I.cnvStatus1 == 0 && I.cnvStatus2 ==1 && I.conveyorStatus == 1)\
)

/*
Cell 2 Test
*/
A[](\
	!(I.cnvStatus1 == 1 && I.cnvStatus2 ==1 && I.conveyorStatus == 1)\
)

/*
Cell 2 Test
*/
A[](\
	!(I.cnvStatus1 == 0 && I.cnvStatus2 ==1 && I.conveyorStatus == 1)\
)

/*
Cell 1 Test
*/
A[](\
	!(I.pad1Sensor == 0 && I.pad2Sensor ==1 && I.robot1Status == 2)\
)

/*
Cell 1 Test
*/
A[](\
	!(I.pad1Sensor == 0 && I.pad2Sensor ==1 && I.robot1Status == 1)\
)

/*
Cell 1 Test
*/
A[](\
	!(I.pad1Sensor == 1 && I.pad2Sensor ==1 && I.robot1Status == 2)\
)

/*
Cell 1 Test
*/
A[](\
	!(I.pad1Sensor == 1 && I.pad2Sensor ==1 && I.robot1Status == 1)\
)

/*
Cell 1 Test
*/
A[](\
	!(I.pad1Sensor == 0 && I.pad2Sensor ==1 && I.robot1Status == 1)\
)
